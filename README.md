# AI Linear Regresion prediction

Trainable AI for linear predictions

## For rust

```bash
cargo test
```

## For asm

```bash
nasm -f elf64 ai_lr.asm && ld -s -o output ai_lr.o && ./output
```

![mathematical-explanation](ai_lr.png)