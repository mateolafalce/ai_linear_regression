section .data

; data for training
x:             ; inputs
   dq  1
   dq  2
   dq  3

y:             ; outputs
   dq  2
   dq  4
   dq  6

size: dq  0.0         ; var for store size-1
dividend: dq 1.0      ; 1/3
divisor: dq 3.0       ; 3 -> size

to_predict: dq 10      ; set any prediction you want  

section .rodata
  msg: db "Prediction: "
  msglen: equ $ - msg
  nl: db 10                  ; \n
  nl_len: equ $ - nl

section .text

global _start
  
_start: 

   call inverse_amount
   call set_variables
   call for_loop
   call set_x_y_
   call iter_for_slope1
   call top_slope_dividend
   call iter_for_slope2
   call top_slope_divisor
   call get_slope
   call get_intercept
   call predict
   call display
   call new_line
   call exit


inverse_amount:

  fld qword [dividend]   ; Load float1 onto the FPU stack
  fld qword [divisor]    ; Add float2 to the value on the FPU stack
  fdiv                   ; a./b.
  fstp qword [size]      ; Store the result back to memory
  ret
    
set_variables:

   mov  rax, 3       ; number bytes to be summed 
   mov  r15, 0       ; r15 = mean of x
   mov  r14, 0       ; r14 = mean of y
   mov  rcx, x       ; rcx will point to the current element to be summed
   mov  rbx, y       ; rcx will point to the current element to be summed
   ret

for_loop:  

   add  r15, [rcx]        ; Σx
   add  r14, [rbx]        ; Σy
   inc  rcx               ; move pointer to next element
   inc  rbx               ; move pointer to next element
   dec  rax               ; decrement counter
   jnz  for_loop          ; if counter not 0, then loop again
   ret

set_x_y_: 

   mov r12, r15      ; Σx -> r12
   mov r11, r14      ; Σy -> r11

   imul r12, size     ; mean x * 1/size 
   imul r11, size     ; mean y * 1/size
   ret

iter_for_slope1: 

   mov  rax, 3        ; number bytes to be summed 
   mov  rcx, x        ; rcx will point to the current element to be summed
   mov  rbx, y        ; rcx will point to the current element to be summed
   mov  r10, 0        ; dividend
   ret

top_slope_dividend: 
   
   mov  r9, [rcx]        ; [rcx] -> r9
   mov  r8, [rbx]        ; [rbx] -> r8

   sub r9, r12           ; xi - Σx
   sub r8, r11           ; yi - Σy

   imul r9, r8           ; (xi - Σx)*(yi - Σy)

   add  r10, r9                  ; concat result
   inc  rcx                      ; move pointer to next element
   inc  rbx                      ; move pointer to next element
   dec  rax                      ; decrement counter
   jnz  top_slope_dividend       ; if counter not 0, then loop again
   ret

iter_for_slope2: 

   mov  rax, 3        ; number bytes to be summed 
   mov  rcx, x        ; rcx will point to the current element to be summed
   mov  r9, 0         ; divisor
   ret

top_slope_divisor: 
   
   mov r8, [rcx]      ; move [xi] to r8
   sub r8, r12        ; xi - Σx
   imul r8, r8        ; (xi - Σx)^2

   add  r9, r8                   ; concat result
   inc  rcx                      ; move pointer to next element
   dec  rax                      ; decrement counter
   jnz  top_slope_divisor        ; if counter not 0, then loop again
   ret

get_slope:

   mov rax, r10    ; set rax
   mov rbx, r9     ; set rbx
   div rbx         ; rax / rbx
   mov r10, rax    ; r10 = slope
   ret

get_intercept:

   mov r9, r14      ; set in r9 -> Σy
   mov r8, r15      ; set in r8 -> Σx
   imul r8, r10     ; Σx * slope
   sub r9, r8       ; Σy - slope * Σx -> r9 = intercept
   ret

predict:

   imul r10, [to_predict]    ; f(x) -> slope*x
   add r10, r9               ; f(x) -> slope*x + intercept. r10 store the result
   mov r12, r10              ; store the result into r12
   ret


display:

   mov rax, 1        ; write(
   mov rdi, 1        ;   STDOUT_FILENO,
   mov rsi, msg      ;   msg,
   mov rdx, msglen   ;   sizeof(msg)
   syscall           ; );
   
   call set_variables_for_convert
   call loop
   ret


set_variables_for_convert:

  mov r15, 20                        ; n iter
  mov r13, 10000000000000000000      ; max divisor, u64::MAX = 18446744073709551615
  mov r11, 0                         ; it will work as a flag for unused zeros
  ret


loop:

  call division

  cmp rax, 0            ; if rax(result of div) == 0
  je check_if_is_need   ; jump to check_if_is_need

  cmp rax, 0            ; if rax(result of div) != 0
  jne modify_flag       ; jump to modify_flag

  check_if_is_need:     
    cmp r11, 0                   ; if the flag is false,
    je jump_the_write            ; jump the write

  modify_flag:
    mov r11, 1            ; change the state

  call write_number
  jump_the_write:
  call modify_number
  call modify_divisor

  dec r15               ; decrement counter
  jnz loop              ; if counter not 0, then loop again
  ret


division:

  mov rax, r12    ; store number content into rax
  mov rbx, r13    ; store the max divisor into rbx
  xor rdx, rdx    ; clear remainder
  div rbx         ; make the division
  mov r14, rax    ; store the result of division into r14
  ret


modify_number:
  mov rax, r14       ; set the result of divison into rax
  mov rbx, r13       ; set the current max divisor into rbx
  mul rbx            ; great_number = result of division * current divisor
  sub r12, rax       ; number - great_number 
  ret


modify_divisor:
  mov rax, r13           ; store divisor content into rax
  mov rbx, 10            ; set 10 into rbx
  xor rdx, rdx    ; clear remainder
  div rbx                ; div divisor/10
  mov r13, rax           ; mov result of division into divisor
  ret


write_number:

  add rax, '0'            ; add +48 to get numbers in ASCII
  mov [to_predict], rax   ; done, store result in "sum"

  mov rax, 1           ; write(
  mov rdi, 1           ;   STDOUT_FILENO,
  mov rsi, to_predict  ;   text,
  mov rdx, 1           ;   sizeof(text)
  syscall              ; );
  ret


new_line:

   mov rax, 1              ; write(
   mov rdi, 1              ;   STDOUT_FILENO,
   mov rsi, nl             ;   \n,
   mov rdx, nl_len         ;   sizeof(\n)
   syscall                 ; );
   ret


exit:
  mov rax, 60       ; exit(
  mov rdi, 0        ;   EXIT_SUCCESS
  syscall           ; );
