#[allow(dead_code)]
struct LinearRegression {
    slope: f64,
    intercept: f64,
}

#[allow(dead_code)]
impl LinearRegression {
    fn train(x: &[f64], y: &[f64]) -> Self {
        let mean_x: f64 = x.iter().sum::<f64>() / x.len() as f64;
        let mean_y: f64 = y.iter().sum::<f64>() / y.len() as f64;
        let numerator: f64 = x
            .iter()
            .zip(y.iter())
            .map(|(xi, yi)| (xi - mean_x) * (yi - mean_y))
            .sum::<f64>();
        let denominator: f64 = x.iter().map(|xi| (xi - mean_x).powi(2)).sum::<f64>();
        let slope: f64 = numerator / denominator;
        let intercept: f64 = mean_y - slope * mean_x;
        LinearRegression { slope, intercept }
    }

    fn predict(&self, x: f64) -> f64 {
        self.slope * x + self.intercept
    }
}

#[cfg(test)]
mod ia {
    use super::*;

    #[test]
    fn ai_train_and_result() {
        let x_train: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0];
        let y_train: Vec<f64> = vec![2.0, 4.0, 6.0, 8.0];
        // We can make a training, then store the LR. So we get a pre-trained system
        let model: LinearRegression = LinearRegression::train(&x_train, &y_train);
        let x_test: f64 = 5.0;
        let y_pred: f64 = model.predict(x_test);
        assert_eq!(y_pred, 10.0);
    }
}
